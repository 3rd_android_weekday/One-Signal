package com.kshrd.onesignal.onesignal;

import android.content.SharedPreferences;
import android.util.Log;
import android.view.Display;

import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationReceivedResult;

import org.json.JSONException;
import org.json.JSONObject;

public class OneSignalNotificationExtender extends NotificationExtenderService {
   @Override
   protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
       // Read properties from result.

      JSONObject object = receivedResult.payload.additionalData;
      String title = "";
      if (object != null){
         try {
            title = object.getString("title");
            SharedPreferences preferences = getSharedPreferences("setting", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("title", title);
            editor.apply();
            return true;
         } catch (JSONException e) {
            e.printStackTrace();
         }
      }


      // Return true to stop the notification from displaying.
      return false;
   }
}