package com.kshrd.onesignal;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OneSignal;

public class MainActivity extends AppCompatActivity {

    Switch swSubscribe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swSubscribe = (Switch) findViewById(R.id.swSubscribe);

        SharedPreferences preferences = getSharedPreferences("setting", MODE_PRIVATE);
        String title = preferences.getString("title", "N/A");
        Toast.makeText(this, title, Toast.LENGTH_SHORT).show();
        getSupportActionBar().setTitle(title);

        if (isSubscribed()){
            swSubscribe.setChecked(true);
        } else {
            swSubscribe.setChecked(false);
        }

        // Check OneSignal Subscription
        swSubscribe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked){
                    // Subscribe OneSignal
                    OneSignal.setSubscription(true);
                } else {
                    OneSignal.setSubscription(false);
                }
            }
        });

    }

    private Boolean isSubscribed() {
         return OneSignal.getPermissionSubscriptionState().getSubscriptionStatus().getSubscribed();
    }
}
