package com.kshrd.onesignal;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by pirang on 6/27/17.
 */

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new OneSignalOpenHandler())
                .setNotificationReceivedHandler(new OneSignalReceiveHandler())
                .init();
    }

    class OneSignalOpenHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {

            JSONObject data = result.notification.payload.additionalData;
            String customKey;

            if (data != null) {
                customKey = data.optString("name", null);
                if (customKey != null)
                    Log.i("DARA_FB", "customkey set with value: " + customKey);
            }

            Intent intent = new Intent(getApplicationContext() , DetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }
    }

    class OneSignalReceiveHandler implements OneSignal.NotificationReceivedHandler{

        @Override
        public void notificationReceived(OSNotification notification) {
            Log.e("OneSignalExample", "notificationReceived!!!!!!");
            Log.e("OneSignalExample", notification.payload.toString());
        }
    }
}
